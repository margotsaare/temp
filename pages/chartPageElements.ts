import { Selector, t } from "testcafe";


class ChartPage {
  NavigatorItem: Selector;
  SMITFirstResultNav: Selector;


  constructor() {
    this.NavigatorItem = Selector('#navigation').find('ul').find('li').nth(1);
    this.SMITFirstResultNav = Selector('body').withText("Valdkonnad");

  }
}
    export default new ChartPage();


