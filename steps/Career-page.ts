import { Given, When, Then } from "cucumber";
import {t, RequestLogger, ClientFunction} from "testcafe";
import ChartPage from "../pages/chartPageElements";
import { config } from '../config';



Given("I am on SMIT\'s career page", async () => {
    await t.navigateTo('https://www.smit.ee/et/karjaar').wait(3000);
});

When('I choose the menu option from navigation', async function() {
    await t.click(ChartPage.NavigatorItem());
    const getLocation = ClientFunction(() => document.location.href);

});

Then('I should be redirected to the right page navigation', async function() {
    await t.expect(ChartPage.SMITFirstResultNav.innerText).contains("Valdkonnad");

});




